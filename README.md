# Docker de OnegeoSuite

Ce dépot est utilisé pour générer les images docker de ONEGEO-SUITE.

Les images sont générées automatiquement par la CI de Gitlab.

## Test de l'image docker en local

Le fichier `docker-compose.yaml` est fourni à titre d'exemple.

Il peut être utilisé pour tester l'image en local.

###### Commandes :

* Lancer les docker

```shell
docker-compose up -d
```

* Tout effacer  (même la base de données)

```shell
docker-compose down --volumes
```
