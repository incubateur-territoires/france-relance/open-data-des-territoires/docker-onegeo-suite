FROM python:3.7

ENV PYTHONUNBUFFERED=1
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal
ENV LC_ALL="C.UTF-8"
ENV LC_CTYPE="C.UTF-8"

RUN apt-get update && \
    apt-get install -y libproj-dev gdal-bin nginx gettext && \
    apt-get install -y --no-install-recommends netcat git && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/

# Set envs
ENV HOME=/home/apprunner
ENV PATH=$HOME/.local/bin:$PATH
ENV APP_PATH=$HOME/onegeo_app

# if WORKDIR only is set, then $APP_PATH will be owned by root :-/
RUN mkdir $HOME $APP_PATH $APP_PATH/config $APP_PATH/media $APP_PATH/static
WORKDIR $APP_PATH

VOLUME $APP_PATH/config $APP_PATH/media $APP_PATH/static

# Upgrade pip
RUN pip install --user --upgrade pip
# Install python requirements
COPY requirements.txt .
COPY docker/nginx/nginx.conf /etc/nginx/sites-enabled/default
RUN pip install --user -r requirements.txt gunicorn python-decouple
COPY . src/

EXPOSE 80
ENTRYPOINT ["src/docker/docker-entrypoint.sh"]
CMD ["src/docker/start.sh"]
