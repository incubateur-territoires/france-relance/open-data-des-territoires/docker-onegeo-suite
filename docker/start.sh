#!/bin/bash

echo "Step 01. starting nginx"

/etc/init.d/nginx restart

echo "Step 02. starting gunicorn"

gunicorn -w ${GUNICORN_MAX_WORKERS:-3} -b 0.0.0.0:5000 config.wsgi:application --timeout ${GUNICORN_TIMEOUT:-1800} --max-requests ${GUNICORN_MAX_REQUEST:-20} 
