"""
Django URL Configuration for ONEGEO-SUITE project.

"""

import urllib3
from six.moves.urllib.parse import urlencode

from django.conf.urls import include
from django.urls import path
from django.conf.urls.i18n import i18n_patterns
from drfreverseproxy.views import ProxyView
from drfreverseproxy.utilites import encode_items


class ServiceProxyView(ProxyView):
    upstream = 'http://localhost'

    def _created_proxy_response(self, request, path):
        request_payload = request.body

        self.log.debug("Request headers: %s", self.request_headers)
        self.upstream = request.GET["url"]
        path = ''
        request_url = (self.upstream + '/' if path and self.upstream[-1] != '/' else self.upstream) + path

        self.log.debug("Request URL: %s", request_url)

        if not self.upstream.startswith('http'):
            raise Exception('Invalid Url')

        if request.GET:
            request.GET._mutable = True
            request.GET.pop('url', None)
            get_data = encode_items(request.GET.lists())
            self.log.debug("get_data: %s", get_data)
            request.GET._mutable = False
            request_url += '&' + urlencode(get_data)
            self.log.debug("Request URL: %s", request_url)

        domain = request_url[8:].split('/')[0]

        if not (domain.endswith('org')
                or domain.endswith('fr')
                or domain.endswith('com')):
            raise Exception('Invalid domain Url')

        try:
            proxy_response = self.http.urlopen(
                request.method,
                request_url,
                redirect=False,
                retries=self.retries,
                headers=self.request_headers,
                body=request_payload,
                decode_content=False,
                preload_content=False
            )
            self.log.debug("Proxy response header: %s", proxy_response.getheaders())
        except urllib3.exceptions.HTTPError as error:
            self.log.exception(error)
            raise

        return proxy_response


urlpatterns = [
    path('service/proxy/', ServiceProxyView.as_view()),
    path('i18n/', include('django.conf.urls.i18n')),
    path('_nested_admin/', include('nested_admin.urls')),
] + i18n_patterns(
    path('usergroup/', include('onegeo_suite.contrib.onegeo_usergroup.urls')),
    path('organisation/', include('onegeo_suite.contrib.onegeo_organisation.urls')),
    path('login/', include('onegeo_suite.contrib.onegeo_login.urls')),
    path('dataset/', include('onegeo_suite.contrib.onegeo_dataset.urls')),
    path('resource/', include('onegeo_suite.contrib.onegeo_resource.urls')),
    path('indexer/', include('onegeo_suite.contrib.onegeo_indexer.urls')),
    path('geoserv/', include('onegeo_suite.contrib.onegeo_geoserv.urls')),
    path('maps/', include('onegeo_suite.contrib.onegeo_maps.urls')),
    path('dashboard/', include('onegeo_suite.api.urls')),
    path('', include('onegeo_suite.urls')),
)
