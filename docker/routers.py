"""
Django routers for ONEGEO-SUITE project.

"""


from decouple import config


DATABASE_FOR_READ = config('DATABASE_FOR_READ', default='default_reader')
DATABASE_FOR_WRITE = config('DATABASE_FOR_WRITE', default='default_writer')


class DefaultRouter(object):

    def db_for_read(self, model, **hints):
        return DATABASE_FOR_READ

    def db_for_write(self, model, **hints):
        return DATABASE_FOR_WRITE

    def allow_relation(self, obj1, obj2, **hints):
        # return obj1._state.db == obj2._state.db
        return True

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        return True
