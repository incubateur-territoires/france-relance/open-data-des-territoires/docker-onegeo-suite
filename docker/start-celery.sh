#!/bin/bash

echo "Step 01. starting celery"

while :
do
    echo "(Re)Starting Celery"
    celery --app=onegeo_suite worker -l INFO
    echo "Celery is dead, restarting"
done

