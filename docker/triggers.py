"""
Django triggers for pigma project.

"""


import logging
from uuid import uuid4

from django.apps import apps
from django.db.models.signals import post_save
from django.dispatch import receiver

from onegeo_suite.contrib.onegeo_dataset.models import Dataset
from onegeo_suite.contrib.onegeo_resource.models import Resource


logger = logging.getLogger(__name__)

CKAN_ID_KEY = 'ckan_id'


@receiver(post_save, sender=Dataset)
@receiver(post_save, sender=Resource)
def add_extra_kvp(sender, instance, created=None, **kwargs):
    if created:
        app_label = sender._meta.app_label
        model_name = sender._meta.model_name

        Extra = apps.get_model(app_label=app_label, model_name='Extra')
        attrs = {
            model_name: instance,
            'key': CKAN_ID_KEY,
            'value': uuid4().__str__(),
        }
        Extra.objects.create(**attrs)
