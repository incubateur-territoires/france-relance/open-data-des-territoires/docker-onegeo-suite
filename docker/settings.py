"""
Django settings for ONEGEO-SUITE project.

"""


from decouple import config
import os
from pathlib import Path

from django.utils.translation import gettext_lazy


# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY', default='CHANGE_ME')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', default=True, cast=bool)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(levelname)-8s %(asctime)s %(message)s [%(pathname)s:%(lineno)d]',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
    },
    'root': {
        'handlers': ['console'],
        'level': config('LOG_LEVEL', default='INFO'),
    },
    'loggers': {
        'django': {
            'handlers': [
                'console',
            ],
            'propagate': True,
            'level': config('LOG_LEVEL', default='INFO'),
        },
    },
}

ALLOWED_HOSTS = config('ALLOWED_HOSTS', default='localhost 127.0.0.1 0.0.0.0').split()

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'prettyjson',
    'django_reverse_admin',
    'drfreverseproxy',
    'nested_admin',
    'rest_framework',
    'guardian',
    'phonenumber_field',
    'taggit',
    'crequest',
    'django_celery_beat',
    'onegeo_suite',
    'onegeo_suite.contrib.onegeo_usergroup',
    'onegeo_suite.contrib.onegeo_organisation',
    'onegeo_suite.contrib.onegeo_login',
    'onegeo_suite.contrib.onegeo_dataset',
    'onegeo_suite.contrib.onegeo_resource',
    'onegeo_suite.contrib.onegeo_datapusher',
    'onegeo_suite.contrib.onegeo_geospatial',
    'onegeo_suite.contrib.onegeo_geoserv',
    'onegeo_suite.contrib.onegeo_geonet',
    'onegeo_suite.contrib.onegeo_indexer',
    'onegeo_suite.contrib.onegeo_maps',
    # 'django_clamd',
    'django_cleanup',
]

PASSWORD_HASHERS = [
    # 'onegeo_suite.contrib.onegeo_geoserv.hashers.GeoserverDigestEncryptor',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

ACCESS_TOKEN_LIFETIME = 3

AUTH_USER_MODEL = 'onegeo_usergroup.User'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'crequest.middleware.CrequestMiddleware',
    'onegeo_suite.contrib.onegeo_login.middleware.TermsRequired',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DEFAULT_DB_ENGINE = 'django.contrib.gis.db.backends.postgis'
DEFAULT_DB_HOST = config('DB_HOST', default='127.0.0.1')
DEFAULT_DB_PORT = config('DB_PORT', default='5432')
DEFAULT_DB_PORT_READONLY = config('DB_PORT_READONLY', default='5433')
DEFAULT_DB_USER = config('DB_USER', default='postgres')
DEFAULT_DB_PASSWORD = config('DB_PWD', default='postgres')
DEFAULT_DB_NAME = config('DB_NAME', default='onegeo_suite')
DATABASE_FOR_READ = config('DATABASE_FOR_READ', default='default_reader')
DATABASE_FOR_WRITE = config('DATABASE_FOR_WRITE', default='default_writer')


DATABASES = {
    'default': {
        'ENGINE': DEFAULT_DB_ENGINE,
        'HOST': DEFAULT_DB_HOST,
        'PORT': DEFAULT_DB_PORT,
        'NAME': DEFAULT_DB_NAME,
        'USER': DEFAULT_DB_USER,
        'PASSWORD': DEFAULT_DB_PASSWORD,
    },
    DATABASE_FOR_WRITE: {
        'ENGINE': DEFAULT_DB_ENGINE,
        'HOST': DEFAULT_DB_HOST,
        'PORT': DEFAULT_DB_PORT,
        'NAME': DEFAULT_DB_NAME,
        'USER': DEFAULT_DB_USER,
        'PASSWORD': DEFAULT_DB_PASSWORD,
    },
    DATABASE_FOR_READ: {
        'ENGINE': DEFAULT_DB_ENGINE,
        'HOST': DEFAULT_DB_HOST,
        'PORT': DEFAULT_DB_PORT_READONLY,
        'NAME': DEFAULT_DB_NAME,
        'USER': DEFAULT_DB_USER,
        'PASSWORD': DEFAULT_DB_PASSWORD,
    },
}


# ATTENTION REGRESSION Cf. https://redmine.neogeo.fr/issues/13091

# DATABASE_ROUTERS = [
#    'config.routers.DefaultRouter'  # ../config/routers.py in this context
# ]

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGES = [
    ('fr', gettext_lazy('French')),
    ('en', gettext_lazy('English')),
]

LANGUAGE_CODE = 'fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

PHONENUMBER_DB_FORMAT = 'NATIONAL'

PHONENUMBER_DEFAULT_REGION = 'FR'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

LOCALE_PATHS = [
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_usergroup/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_organisation/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_login/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_dataset/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_resource/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_datapusher/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_geospatial/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_geoserv/conf/locale',
    # f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_indexer/conf/locale',
    f'{BASE_DIR}/src/django-onegeo-suite/onegeo_suite/contrib/onegeo_maps/conf/locale',
]

# E-mail and notification parameters
EMAIL_BACKEND = config('EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST = config('EMAIL_HOST', default='')
EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='')
EMAIL_PORT = config('EMAIL_PORT', cast=int, default=25)
EMAIL_USE_TLS = config('EMAIL_USE_TLS', cast=bool, default=True)
DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL', default='')

ONEGEOSUITE_EMAIL_SUBJECT_PREFIX = config('EMAIL_SUBJECT_PREFIX', default='ONEGEO-SUITE |')
ONEGEOSUITE_EMAIL_FOOTER = config('EMAIL_FOOTER', default=None)

# Static and media files
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Onegeo-Suite triggers:

ONEGEOSUITE_TRIGGERS = [
    'config.triggers',
]

# Celery settings:

REDIS_HOST = config('REDIS_HOST', default='127.0.0.1')
REDIS_PORT = config('REDIS_PORT', default=6379)
REDIS_BASE = config('REDIS_BASE', default=0)

ONEGEOSUITE_CELERY_BROKER_URL = f'redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_BASE}'
ONEGEOSUITE_CELERY_RESULT_BACKEND = f'redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_BASE}'
ONEGEOSUITE_CELERY_ACCEPT_CONTENT = ['pickle', ]
ONEGEOSUITE_CELERY_TASK_PROTOCOL = 1
ONEGEOSUITE_CELERY_TASK_SERIALIZER = 'pickle'
ONEGEOSUITE_CELERY_RESULT_SERIALIZER = 'pickle'
ONEGEOSUITE_CELERY_TIMEZONE = TIME_ZONE
ONEGEOSUITE_CELERY_BEAT_SCHEDULER = 'onegeo_suite.schedulers.DatabaseScheduler'

# Guardian settings:

GUARDIAN_MONKEY_PATCH = False
GUARDIAN_GET_CONTENT_TYPE = 'onegeo_suite.core.ctypes.get_content_type'
ANONYMOUS_USER_NAME = 'anonymous'
GUARDIAN_GET_INIT_ANONYMOUS_USER = 'onegeo_suite.contrib.onegeo_usergroup.models.get_anonymous_user_instance'

# DRF settings:

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'onegeo_suite.contrib.onegeo_login.authentication.SimpleTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'onegeo_suite.contrib.onegeo_login.authentication.JWTAuthentication',
    ],
    'URL_FORMAT_OVERRIDE': 'drf_format',
}

# Onegeo-suite site-admin settings:

ONEGEOSUITE_PUBLIC_BASE_URL = config('PUBLIC_BASE_URL', default='http://127.0.0.1:8000')

ONEGEOSUITE_SITE_URL = '/onegeo/admin/'
ONEGEOSUITE_SITE_LOGIN_URI = '/onegeo/admin/login/'
ONEGEOSUITE_SITE_LOGOUT_URI = '/onegeo/admin/logout/'

# onegeo-resource settings:

ONEGEO_RESOURCE_FTP_HOST = config('FTP_HOST', default='127.0.0.1')
ONEGEO_RESOURCE_FTP_PORT = config('FTP_PORT', default=22)
ONEGEO_RESOURCE_FTP_SECURE = config('FTP_SECURE', default=True)
ONEGEO_RESOURCE_FTP_BASE_DIR = config('FTP_BASE_DIR', default='/groups')
ONEGEO_RESOURCE_FTP_GROUP_PREFIX = config('FTP_GROUP_PREFIX', default='usergroup')
ONEGEO_RESOURCE_FTP_DROP_DIR = config('FTP_DROP_DIR', default='drop_here')

# onegeo-datapusher settings:

ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_TYPE = 'postgresql'
ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_HOST = config('DATAPUSHER_DB_HOST', default=DEFAULT_DB_HOST)
ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_PORT = config('DATAPUSHER_DB_PORT', default=DEFAULT_DB_PORT)
ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_NAME = config('DATAPUSHER_DB_NAME', default=DEFAULT_DB_NAME)
ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_USERNAME = config('DATAPUSHER_DB_USER', default=DEFAULT_DB_USER)
ONEGEOSUITE_DATAPUSHER_DATASTORE_DB_PASSWORD = config('DATAPUSHER_DB_PWD', default=DEFAULT_DB_PASSWORD)

# onegeo-geoserv settings:

ONEGEOSUITE_GEOSERV_OWS_EXTERNAL_URL = config('GEOSERVER_OWS_EXTERNAL_URL', default='http://127.0.0.1/fr/geoserv/{workspace}/ows/')
ONEGEOSUITE_GEOSERV_GWC_UPSTREAM = config('GEOSERVER_GWC_UPSTREAM', default='http://127.0.0.1/geoserver/gwc/service/wmts/')
ONEGEOSUITE_GEOSERV_OWS_UPSTREAM = config('GEOSERVER_OWS_UPSTREAM', default='http://127.0.0.1/geoserver/')
ONEGEOSUITE_GEOSERV_REST_LOCATION = config('GEOSERVER_LOCATION', default='http://127.0.0.1/geoserver/rest/')
ONEGEOSUITE_GEOSERV_REST_USERNAME = config('GEOSERVER_USER', default='admin')
ONEGEOSUITE_GEOSERV_REST_PASSWORD = config('GEOSERVER_PWD', default='geoserver')

# onegeo-geonet:

ONEGEOSUITE_GEONETWORK_CATALOG_UUID = config('GEONETWORK_CATALOG_UUID', '00000000-0000-0000-0000-000000000000')
ONEGEOSUITE_GEONETWORK_EXTERNAL_URL = config('GEONETWORK_EXTERNAL_URL', 'http://127.0.0.1/geonetwork/')
ONEGEOSUITE_GEONETWORK_SRV_BASE_URL = config('GEONETWORK_SRV_BASE_URL', 'http://127.0.0.1/geonetwork/srv/')
ONEGEOSUITE_GEONETWORK_USERNAME = config('GEONETWORK_USERNAME', 'admin')
ONEGEOSUITE_GEONETWORK_PASSWORD = config('GEONETWORK_PASSWORD', 'admin')

# onegeo-indexer:

ONEGEOSUITE_INDEXER_ELASTIC_UPSTREAM = config('INDEXER_ELASTIC_UPSTREAM', default='http://127.0.0.1/elastic/')
ONEGEOSUITE_INDEXER_ELASTIC_CONNECTION = [
    {
        'host': config('INDEXER_ELASTIC_HOST', default='127.0.0.1'),
        'port': int(config('INDEXER_ELASTIC_PORT', default='9200')),
        'url_prefix': config('INDEXER_ELASTIC_PREFIX', default='elastic'),
        'use_ssl': config('INDEXER_ELASTIC_SSL', default='0') == '1',
        'timeout': int(config('INDEXER_ELASTIC_TIMEOUT', default=120)),
        'retry_on_timeout': True,
        'max_retries': 4,
    },
]
ONEGEOSUITE_INDEXER_ELASTIC_ALIAS_ANONYMOUS = 'public'
ONEGEOSUITE_INDEXER_ELASTIC_INDEX = config('INDEXER_ELASTIC_INDEX', default='portail_data')
ONEGEOSUITE_INDEXER_ELASTIC_REPLICAS = config('INDEXER_ELASTIC_REPLICAS', default='0')
ONEGEOSUITE_INDEXER_ELASTIC_SHARDS = config('INDEXER_ELASTIC_SHARDS', default='1')
ONEGEOSUITE_INDEXER_EXPORT_DIR = Path(MEDIA_ROOT) / config('INDEXER_EXPORT_DIR', default='indexer_exports')

# onegeo-login:

ONEGEOSUITE_LOGIN_SIGNUP_CONFIRMATION_PUBLIC_URL = ONEGEOSUITE_PUBLIC_BASE_URL + '/onegeo-login/validateregistration/'
ONEGEOSUITE_LOGIN_FORGOTTEN_PASSWORD_PUBLIC_URL = ONEGEOSUITE_PUBLIC_BASE_URL + '/onegeo-login/reinitpwd/'
ONEGEOSUITE_LOGIN_EMAIL_CONFIRMATION_PUBLIC_URL = ONEGEOSUITE_PUBLIC_BASE_URL + '/onegeo-login/validate-email/'
ONEGEOSUITE_LOGIN_USER_ACCOUNT_PUBLIC_URL = ONEGEOSUITE_PUBLIC_BASE_URL + '/onegeo-publish/accounts/users/'

# misc onegeo settings:

ONEGEOSUITE_PUBLIC_PORTAL_DATASET_PATTERN_URL = config('PUBLIC_PORTAL_DATASET_PATTERN_URL', 'http://127.0.0.1/portail/fr/jeux-de-donnees/{dataset}/')

